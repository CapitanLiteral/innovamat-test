﻿using System;
using UnityEngine;

public class ButtonNumberController : MonoBehaviour
{
	[SerializeField] private ButtonNumberModel _buttonNumberModel;
	public ButtonNumberModel NumberModel => _buttonNumberModel;
	public Action<ButtonNumberController> ButtonClicked;
	public void SetNumber(int number, bool isCorrect)
	{
		_buttonNumberModel.Number = number;
		_buttonNumberModel.Text = number.ToString();
		_buttonNumberModel.IsCorrect = isCorrect;
	}
	public void ShowButtonResult() { }
	public void MakeVisible(bool visible) { _buttonNumberModel.Visible = visible; }
	public void OnClick()
	{
		ButtonClicked?.Invoke(this);
		if (_buttonNumberModel.IsCorrect)
		{
			_buttonNumberModel.Color = _buttonNumberModel.Correct;
		}
		else
		{
			_buttonNumberModel.Color = _buttonNumberModel.Incorrect;
		}

		_buttonNumberModel.Interactable = false;
	}
	public void PrepareButton() { _buttonNumberModel.Color = _buttonNumberModel.PendingClick; }
}