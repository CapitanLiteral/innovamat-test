﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonNumberModel : MonoBehaviour
{
	[SerializeField] private Color _correct;
	[SerializeField] private Color _incorrect;
	[SerializeField] private Color _pendingClick;

	public Color Color
	{
		get { return _color; }
		set
		{
			_color = value;
			_colorChanged?.Invoke(_color);
		}
	}
	public event Action<Color> ColorChanged
	{
		add
		{
			_colorChanged += value;
			value(_color);
		}
		remove
		{
			if (_colorChanged != null) _colorChanged -= value;
		}
	}
	private Color _color;
	private Action<Color> _colorChanged;

	public string Text 
	{
		get
		{
			return _text;
		}
		set
		{
			_text = value;
			_textChanged?.Invoke(_text);
		}
	}
	public event Action<string> TextChanged
	{
		add
		{
			_textChanged += value;
			value(_text);
		}
		remove
		{
			if (_textChanged != null) _textChanged -= value;
		}
	}
	private string _text;
	private Action<string> _textChanged;

	public bool Visible
	{
		get { return _visible; }
		set
		{
			_visible = value;
			_visibleChanged?.Invoke(_visible);
		}
	}
	public event Action<bool> VisibleChanged
	{
		add
		{
			_visibleChanged += value;
			value(_visible);
		}
		remove
		{
			if (_visibleChanged != null) _visibleChanged -= value;
		}
	}
	private bool _visible;
	private Action<bool> _visibleChanged;
	public Action<bool> OnVisibleFinishedAnimation { get; set; }

	public int Number { get; set; }
	public bool IsCorrect { get; set; }
	public Color Correct => _correct;
	public Color Incorrect => _incorrect;
	public Color PendingClick => _pendingClick;
	public bool Interactable
	{
		get { return _interactable; }
		set
		{
			_interactable = value;
			_interactableChanged?.Invoke(_interactable);
		}
	}
	public event Action<bool> InteractableChanged
	{
		add
		{
			_interactableChanged += value;
			value(_interactable);
		}
		remove
		{
			if (_interactableChanged != null) _interactableChanged -= value;
		}
	}
	private bool _interactable;
	private Action<bool> _interactableChanged;

}
