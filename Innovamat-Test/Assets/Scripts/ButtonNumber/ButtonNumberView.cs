﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonNumberView : MonoBehaviour
{
	[SerializeField] private Image _image;
	[SerializeField] private Button _button;
	
	[SerializeField] private ButtonNumberModel _buttonNumberModel;
	[SerializeField] private CanvasGroup _canvasGroup;
	[SerializeField, Tooltip("Y values between 0 and 1")]
	private AnimationCurve _animationCurve;

	private Text _textComponent;
	private void Awake()
	{
		_textComponent = GetComponentInChildren<Text>();
		_canvasGroup.alpha = 0;
	}
	private void Start()
	{
		_buttonNumberModel.TextChanged += UpdateText;
		_buttonNumberModel.VisibleChanged += ShowTextAnimation;
		_buttonNumberModel.ColorChanged += OnColorChanged;
		_buttonNumberModel.InteractableChanged += OnInteractableChanged;
	}
	private void OnInteractableChanged(bool obj) { _button.interactable = obj; }
	private void UpdateText(string text)
	{
		_textComponent.text = text;
	}

	private void ShowTextAnimation(bool visible) { StartCoroutine(ShowTextAnimationCr(visible)); }
	private IEnumerator ShowTextAnimationCr(bool visible)
	{
		
		float t = 0;
		float animationTimeLength = _animationCurve[_animationCurve.length - 1].time;
		float TOLERANCE = 0.05f;
		if (visible)
		{
			if (Math.Abs(_canvasGroup.alpha - _animationCurve.Evaluate(animationTimeLength)) > TOLERANCE)
			{
				_buttonNumberModel.Interactable = false;
				while (t < animationTimeLength)
				{
					_canvasGroup.alpha = _animationCurve.Evaluate(t);
					t += Time.deltaTime;
					yield return null;
				}

				_canvasGroup.alpha = _animationCurve.Evaluate(animationTimeLength);
				_buttonNumberModel.OnVisibleFinishedAnimation?.Invoke(visible);
				_buttonNumberModel.Interactable = true;
			}
		}
		else if(Math.Abs(_canvasGroup.alpha - (1 - _animationCurve.Evaluate(animationTimeLength))) > TOLERANCE)
		{
			_buttonNumberModel.Interactable = false;
			while (t < animationTimeLength)
			{
				_canvasGroup.alpha = 1-_animationCurve.Evaluate(t);
				t += Time.deltaTime;
				yield return null;
			}

			_canvasGroup.alpha = 1-_animationCurve.Evaluate(animationTimeLength);
			_buttonNumberModel.OnVisibleFinishedAnimation?.Invoke(visible);
		}

		
	}
	private void OnColorChanged(Color color)
	{
		_image.color = color;
	}
}
