﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CounterController : MonoBehaviour
{
    [SerializeField] private CounterModel _counterModel;
    
    public void IncreaseCounter()
    {
        _counterModel.Number++;
    }
    public void ResetCounter()
    {
        _counterModel.Number = 0;
    }
}
