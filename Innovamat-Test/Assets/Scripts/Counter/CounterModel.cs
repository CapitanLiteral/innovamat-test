﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CounterModel : MonoBehaviour
{
    [SerializeField] public Color _color;
    [SerializeField] private string _text;
    public string Text
    {
        get { return _text; }
        set
        {
            _text = value;
            _textChanged?.Invoke(_text);
        }
    }
    public event Action<string> TextChanged
    {
        add
        {
            _textChanged += value;
            value(_text);
        }
        remove
        {
            if (_textChanged != null) _textChanged -= value;
        }
    }
    private Action<string> _textChanged;  
    
    private int _number;
    public int Number
    {
        get { return _number; }
        set
        {
            _number = value;
            __numberChanged?.Invoke(_number);
        }
    }
    public event Action<int> NumberChanged
    {
        add
        {
            __numberChanged += value;
            value(_number);
        }
        remove
        {
            if (__numberChanged != null) __numberChanged -= value;
        }
    }
    private Action<int> __numberChanged;

    
}
