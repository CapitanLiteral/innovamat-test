﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CounterView : MonoBehaviour
{
    [SerializeField] private CounterModel _counterModel;
    [SerializeField] private Text _text;
    [SerializeField] private Text _number;
    
    private void Start()
    {
        _counterModel.TextChanged += UpdateText;
        _counterModel.NumberChanged += NumberChanged;
        _number.color = _counterModel._color;
    }
    private void NumberChanged(int obj)
    {
        _number.text = obj.ToString();
    }
    private void UpdateText(string obj)
    {
        _text.text = obj;
    }
}
