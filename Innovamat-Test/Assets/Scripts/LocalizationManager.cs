﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;

public class LocalizationManager : IDependency
{
	private Dictionary<string, LanguageInfo> _availableLanguages = new Dictionary<string, LanguageInfo>();
	public LocalizationManager()
	{
		DependencyManager.AddDependency(this);

		Init();
	}
	public Dictionary<string, LanguageInfo> AvailableLanguages => _availableLanguages;
	private void Init()
	{
		//To deserialize each language maybe it could go as an implementation per language as it is the number conversion.
		var numbersFile = Resources.Load("numbers") as TextAsset;

		var jobject = JObject.Parse(numbersFile.text);
		foreach (var availableLanguage in jobject["available_languages"])
		{
			//Fast coding, but watch out with namespaces and stuff, this should need rework in further iterations
			var languageType = Type.GetType($"Language{availableLanguage}");
			if (languageType != null)
			{
				var languageInfo = Activator.CreateInstance(languageType) as LanguageInfo;
				_availableLanguages.Add(availableLanguage.ToString(), languageInfo);
				if (languageInfo != null)
				{
					string languageKey = availableLanguage.Value<string>();
					JToken languageToken = jobject[languageKey];

					languageInfo.maxNumber = (int) languageToken["max_supported_number"];
					languageInfo.units = JsonConvert.DeserializeObject<List<string>>(languageToken["units"].ToString());
					foreach (var specialNumber in languageToken["special_numbers"].ToObject<JObject>())
					{
						languageInfo.specialNumbers.Add(Convert.ToInt32(specialNumber.Key),
							specialNumber.Value.ToString());
					}

					foreach (var ten in languageToken["tens"].ToObject<JObject>())
					{
						string single = ten.Value["single"].ToString();
						string comp = ten.Value["compound"].ToString();
						string conj = ten.Value["conjunction"].ToString();
						languageInfo.tens.Add(new LanguageInfo.NumInfo(single, comp, conj));
					}

					foreach (var ten in languageToken["hundreds"].ToObject<JObject>())
					{
						string single = ten.Value["single"].ToString();
						string comp = ten.Value["compound"].ToString();
						string conj = ten.Value["conjunction"].ToString();
						languageInfo.hundreds.Add(new LanguageInfo.NumInfo(single, comp, conj));
					}
				}
			}
		}
	}
}