﻿using UnityEngine;

public static class IntExtensions
{
	//Might look silly but it's the most optimal way I found to get number of digits
	public static int GetNumDigits(this int n)
	{
		if (n >= 0)
		{
			if (n < 10) return 1;
			if (n < 100) return 2;
			if (n < 1000) return 3;
			if (n < 10000) return 4;
			if (n < 100000) return 5;
			return 6;
		}
		else
		{
			if (n > -10) return 2;
			if (n > -100) return 3;
			if (n > -1000) return 4;
			if (n > -10000) return 5;
			return 6;
		}
	}
	public static int GetPlace(this int value, int place)
	{
		int n = (int) Mathf.Pow(10, place);
		return ((value % (n * 10)) - (value % n)) / n;
	}
	public static int GetPlaceNumber(this int value, int place)
	{
		int n = (int) Mathf.Pow(10, place);
		return ((value % (n * 10)) - (value % n));
	}
}