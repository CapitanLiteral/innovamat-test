﻿using UnityEngine;

public class TextNumberController : MonoBehaviour
{
	[SerializeField] private TextNumberModel _textNumberModel;
	[Header("----- TEST -----")] [SerializeField]
	private int _forcedNumber;

	public TextNumberModel NumberModel { get => _textNumberModel; set => _textNumberModel = value; }

	public void SetText(string text) { _textNumberModel.Text = text; }
	public void MakeVisible(bool visible) { _textNumberModel.Visible = visible; }

	[ContextMenu("Force TestNumber")]
	public void ForceNumber()
	{
		//Silly :/
		var lang = DependencyManager.GetDependency<LocalizationManager>()
			.AvailableLanguages[FindObjectOfType<Game>().Language];
		_textNumberModel.Text = lang.GetNumber(_forcedNumber);
	}
}