﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextNumberModel : MonoBehaviour
{
    public string Text
    {
        get { return _text; }
        set
        {
            _text = value;
            _textChanged?.Invoke(_text);
        }
    }
    public event Action<string> TextChanged
    {
        add
        {
            _textChanged += value;
            value(_text);
        }
        remove
        {
            if (_textChanged != null) _textChanged -= value;
        }
    }
    private string _text = "asdasdadasdads";
    private Action<string> _textChanged;


    public bool Visible
    {
        get { return _visible; }
        set
        {
            _visible = value;
            _visibleChanged?.Invoke(_visible);
        }
    }
    public event Action<bool> VisibleChanged
    {
        add
        {
            _visibleChanged += value;
            value(_visible);
        }
        remove
        {
            if (_visibleChanged != null) _visibleChanged -= value;
        }
    }
    private bool _visible;
    private Action<bool> _visibleChanged;
    public Action<bool> OnVisibleFinishedAnimation { get; set; }
}
