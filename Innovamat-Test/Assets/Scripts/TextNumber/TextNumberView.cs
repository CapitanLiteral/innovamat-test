﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextNumberView : MonoBehaviour
{
	[SerializeField] private TextNumberModel _textNumberModel;
	[SerializeField] private CanvasGroup _canvasGroup;
	[SerializeField, Tooltip("Y values between 0 and 1")]
	private AnimationCurve _animationCurve;

	private Text _textComponent;
	private void Awake() { _textComponent = GetComponentInChildren<Text>(); }
	private void Start()
	{
		_canvasGroup.alpha = 0;
		_textNumberModel.TextChanged += UpdateText;
		_textNumberModel.VisibleChanged += ShowTextAnimation;
		
	}
	private void UpdateText(string text) { _textComponent.text = text; }
	private void ShowTextAnimation(bool visible) { StartCoroutine(ShowTextAnimationCr(visible)); }
	private IEnumerator ShowTextAnimationCr(bool visible)
	{
		float t = 0;
		float animationTimeLength = _animationCurve[_animationCurve.length - 1].time;
		float TOLERANCE = 0.05f;
		if (visible)
		{
			if (Math.Abs(_canvasGroup.alpha - _animationCurve.Evaluate(animationTimeLength)) > TOLERANCE)
			{
				while (t < animationTimeLength)
				{
					_canvasGroup.alpha = _animationCurve.Evaluate(t);
					t += Time.deltaTime;
					yield return null;
				}

				_canvasGroup.alpha = _animationCurve.Evaluate(animationTimeLength);
				_textNumberModel.OnVisibleFinishedAnimation?.Invoke(visible);
			}
		}
		else if (Math.Abs(_canvasGroup.alpha - (1 - _animationCurve.Evaluate(animationTimeLength))) > TOLERANCE)
		{
			while (t < animationTimeLength)
			{
				_canvasGroup.alpha = 1 - _animationCurve.Evaluate(t);
				t += Time.deltaTime;
				yield return null;
			}

			_canvasGroup.alpha = 1 - _animationCurve.Evaluate(animationTimeLength);
			_textNumberModel.OnVisibleFinishedAnimation?.Invoke(visible);
		}
	}
}