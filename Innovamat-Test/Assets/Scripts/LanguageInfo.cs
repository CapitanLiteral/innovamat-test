﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public abstract class LanguageInfo
{
	public string Id;
	public int maxNumber;
	public List<string> units;
	public List<NumInfo> tens = new List<NumInfo>();
	public List<NumInfo> hundreds = new List<NumInfo>();
	public List<NumInfo> thousands = new List<NumInfo>();
	public Dictionary<int, string> specialNumbers = new Dictionary<int, string>();

	public abstract string GetNumber(int number);

	public struct NumInfo
	{
		public string single;
		public string compound;
		public string conjunction;
		public NumInfo(string single, string compound, string conjunction = "")
		{
			this.single = single;
			this.compound = compound;
			this.conjunction = conjunction;
		}
	}
}

public class LanguageCat : LanguageInfo
{
	public override string GetNumber(int number)
	{
		if (number > maxNumber) throw new Exception("Max number exceeded");
		if (number < 10) return units[number];
		if (specialNumbers.ContainsKey(number)) return specialNumbers[number];

		List<int> slicedNum = new List<int>();
		for (int i = 0; i < number.GetNumDigits(); i++)
		{
			slicedNum.Add(number.GetPlace(i));
		}

		string stringNumber = "";
		for (int i = 0; i < slicedNum.Count; i++)
		{
			switch (i)
			{
				case 0:
				{
					if (slicedNum[i] == 0) break;
					stringNumber = units[slicedNum[0]];
					break;
				}
				case 1:
				{
					if (slicedNum[i] == 0) break;
					//TODO: improve this, it is not safe enough if this scales to bigger numbers in the future
					int possibleSpecialNumber = 0;
					for (int j = 0; j <= i; j++)
					{
						possibleSpecialNumber += number.GetPlaceNumber(j);
					}

					if (specialNumbers.ContainsKey(possibleSpecialNumber))
					{
						stringNumber = specialNumbers[possibleSpecialNumber];
					}
					//--
					else if (slicedNum[i - 1] == 0)
						stringNumber = tens[slicedNum[i] - 1].single;
					else
					{
						stringNumber = stringNumber.Insert(0,
							tens[slicedNum[1] - 1].compound + tens[slicedNum[1] - 1].conjunction);
					}

					break;
				}
				case 2:
				{
					if (slicedNum[i] == 0) break;
					if (slicedNum[i - 1] == 0 && stringNumber == "")
					{
						stringNumber = slicedNum[i] == 1
							? hundreds[0].single
							: $"{units[slicedNum[i]]}{hundreds[0].conjunction}{hundreds[0].compound}";
					}

					else
					{
						string str = "";
						if (slicedNum[i] == 1)
							str = hundreds[0].single + " ";
						else
							str = $"{units[slicedNum[i]]}{hundreds[0].conjunction}{hundreds[0].compound} ";
						stringNumber = stringNumber.Insert(0, str);
					}

					break;
				}
			}
		}

		return stringNumber;
	}
}

public class LanguageEng : LanguageInfo
{
	//TODO: This is same as Cat, just as placeholder
	public override string GetNumber(int number) {
		if (number > maxNumber) throw new Exception("Max number exceeded");
		if (number < 10) return units[number];
		if (specialNumbers.ContainsKey(number)) return specialNumbers[number];

		List<int> slicedNum = new List<int>();
		for (int i = 0; i < number.GetNumDigits(); i++)
		{
			slicedNum.Add(number.GetPlace(i));
		}

		string stringNumber = "";
		for (int i = 0; i < slicedNum.Count; i++)
		{
			switch (i)
			{
				case 0:
				{
					if (slicedNum[i] == 0) break;
					stringNumber = units[slicedNum[0]];
					break;
				}
				case 1:
				{
					if (slicedNum[i] == 0) break;
					//TODO: improve this, it is not safe enough if this scales to bigger numbers in the future
					int possibleSpecialNumber = 0;
					for (int j = 0; j <= i; j++)
					{
						possibleSpecialNumber += number.GetPlaceNumber(j);
					}

					if (specialNumbers.ContainsKey(possibleSpecialNumber))
					{
						stringNumber = specialNumbers[possibleSpecialNumber];
					}
					//--
					else if (slicedNum[i - 1] == 0)
						stringNumber = tens[slicedNum[i] - 1].single;
					else
					{
						stringNumber = stringNumber.Insert(0,
							tens[slicedNum[1] - 1].compound + tens[slicedNum[1] - 1].conjunction);
					}

					break;
				}
				case 2:
				{
					if (slicedNum[i] == 0) break;
					if (slicedNum[i - 1] == 0 && stringNumber == "")
					{
						stringNumber = slicedNum[i] == 1
							? hundreds[0].single
							: $"{units[slicedNum[i]]}{hundreds[0].conjunction}{hundreds[0].compound}";
					}

					else
					{
						string str = "";
						if (slicedNum[i] == 1)
							str = hundreds[0].single + " ";
						else
							str = $"{units[slicedNum[i]]}{hundreds[0].conjunction}{hundreds[0].compound} ";
						stringNumber = stringNumber.Insert(0, str);
					}

					break;
				}
			}
		}

		return stringNumber;
	}
}