﻿using System;
using System.Collections.Generic;

/// <summary>
/// Single instance dependency manager
/// </summary>
public sealed class DependencyManager
{
	public static DependencyManager Instance => instance ?? (instance = new DependencyManager());
	private static DependencyManager instance;
	private readonly Dictionary<Type, IDependency> _dependencies = new Dictionary<Type, IDependency>();
	private DependencyManager() { }

	public static void AddDependency(IDependency dependency)
	{
		if (Instance._dependencies.ContainsKey(dependency.GetType()))
		{
			throw new Exception("Dependency already contained");
		}

		Instance._dependencies.Add(dependency.GetType(), dependency);
	}
	public static void RemoveDependency(IDependency dependency) { Instance._dependencies.Remove(dependency.GetType()); }
	public static T GetDependency<T>() where T : IDependency
	{
		foreach (var dependency in Instance._dependencies)
		{
			if (dependency.Value is T output) return output;
		}

		throw new Exception("Trying to get non existent instance");
	}
}