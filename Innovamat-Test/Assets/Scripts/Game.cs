﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using Random = System.Random;

public class Game : MonoBehaviour
{
	[SerializeField] private string _language = "Cat";
	[SerializeField] private TextNumberController _textNumberController;
	[SerializeField] private List<ButtonNumberController> _buttonNumberControllers;
	[SerializeField] private LocalizationManager _localizationManager;
	[SerializeField] private CounterController _correctCounter;
	[SerializeField] private CounterController _wrongCounter;
	
	private int correctNumber;
	private ButtonNumberController correctNumberButon;
	public GameStateEnum GameState { get; private set; }
	public string Language => _language;

	public enum GameStateEnum
	{
		PrepareGame,
		ShowNumber,
		ShowButtons,
		WaitForPlayerInput,
		ShowCorrectAnswer,
		HideAll
	}

	private void Awake()
	{
		_localizationManager = new LocalizationManager();
	}
	private void Start()
	{
		GameState = GameStateEnum.PrepareGame;
		StartCoroutine(GameFsm());
	}

	private IEnumerator GameFsm()
	{
		int GetRandomNumberOfSameDigitCount(int numDigits)
		{
			return UnityEngine.Random.Range((int) Mathf.Pow(10f, numDigits-1),
				(int) Mathf.Pow(10, numDigits));
		}
		_correctCounter.ResetCounter();
		_wrongCounter.ResetCounter();
		while (true)
		{
			switch (GameState)
			{
				case GameStateEnum.PrepareGame:
				{
					int maxNumber = _localizationManager.AvailableLanguages[_language].maxNumber;
					correctNumber = UnityEngine.Random.Range(0, maxNumber + 1);
					int[] badNumbers = new int[2];
					badNumbers[0] = GetRandomNumberOfSameDigitCount(correctNumber.GetNumDigits());
					do
					{
						badNumbers[1] = GetRandomNumberOfSameDigitCount(correctNumber.GetNumDigits());
					} while (badNumbers[0] == badNumbers[1]);

					_textNumberController.SetText(_localizationManager.AvailableLanguages[_language]
						.GetNumber(correctNumber));
					int randomButton = UnityEngine.Random.Range(0, 3);
					int i = 0, j = 0;
					foreach (var buttonNumberController in _buttonNumberControllers)
					{
						buttonNumberController.PrepareButton();
						if (i == randomButton)
						{
							buttonNumberController.SetNumber(correctNumber, true);
							correctNumberButon = buttonNumberController;
						}
						else
						{
							buttonNumberController.SetNumber(badNumbers[j++], false);
						}

						i++;
					}

					GameState = GameStateEnum.ShowNumber;
					break;
				}
				case GameStateEnum.ShowNumber:
				{
					Debug.Log("ShowNumber");
					_textNumberController.MakeVisible(true);
					bool buttonVisible = false;
					_textNumberController.NumberModel.OnVisibleFinishedAnimation += WaitTextAnimation;
					yield return new WaitUntil(() => buttonVisible);
					GameState = GameStateEnum.ShowButtons;
					break;

					void WaitTextAnimation(bool visible)
					{
						Debug.Log("ShowNumberFinished");
						if (_textNumberController.NumberModel.OnVisibleFinishedAnimation != null)
							_textNumberController.NumberModel.OnVisibleFinishedAnimation -= WaitTextAnimation;
						buttonVisible = true;
					}
				}
				case GameStateEnum.ShowButtons:
				{
					Debug.Log("ShowButtons");
					int buttonsVisible = 0;
					foreach (var buttonNumberController in _buttonNumberControllers)
					{
						buttonNumberController.MakeVisible(true);
						buttonNumberController.NumberModel.OnVisibleFinishedAnimation += WaitButonsAnimation;
					}

					yield return new WaitUntil(() => buttonsVisible == _buttonNumberControllers.Count);
					GameState = GameStateEnum.WaitForPlayerInput;
					break;

					void WaitButonsAnimation(bool visible)
					{
						Debug.Log("ShowButtonFinished");
						if (_textNumberController.NumberModel.OnVisibleFinishedAnimation != null)
							_textNumberController.NumberModel.OnVisibleFinishedAnimation -= WaitButonsAnimation;
						buttonsVisible++;
					}
				}
				case GameStateEnum.WaitForPlayerInput:
				{
					int misses = 0;
					Debug.Log("Waiting player input");
					bool correctAnswerClicked = false;
					foreach (var buttonNumberController in _buttonNumberControllers)
					{
						buttonNumberController.ButtonClicked += WaitForInput;
					}

					yield return new WaitUntil(() => correctAnswerClicked || misses == 2);

					foreach (var buttonNumberController in _buttonNumberControllers)
					{
						if (buttonNumberController.ButtonClicked != null)
							buttonNumberController.ButtonClicked -= WaitForInput;
					}

					if (misses == 2)
					{
						_wrongCounter.IncreaseCounter();
					}

					if (correctAnswerClicked)
					{
						_correctCounter.IncreaseCounter();
					}

					GameState = GameStateEnum.ShowCorrectAnswer;
					break;

					void WaitForInput(ButtonNumberController button)
					{
						if (button.NumberModel.IsCorrect)
						{
							correctAnswerClicked = true;
						}
						else
						{
							misses++;
							if (misses == 1)
							{
								button.MakeVisible(false);
							}
						}
					}
				}
				case GameStateEnum.ShowCorrectAnswer:
					Debug.Log("Show Corect answer");
					foreach (var buttonNumberController in _buttonNumberControllers)
						buttonNumberController.NumberModel.Interactable = false;
					correctNumberButon.NumberModel.Color = correctNumberButon.NumberModel.Correct;
					GameState = GameStateEnum.HideAll;
					break;
				case GameStateEnum.HideAll:
				{
					Debug.Log("Hide all");
					int buttonsVisible = 0;
					int buttonsToHide = 0;
					bool textVisible = false;
					foreach (var buttonNumberController in _buttonNumberControllers)
					{
						if (buttonNumberController.NumberModel.Visible)
						{
							buttonNumberController.NumberModel.OnVisibleFinishedAnimation += WaitButtonsAnimation;
							buttonNumberController.MakeVisible(false);
							buttonsToHide++;
						}
					}

					_textNumberController.NumberModel.OnVisibleFinishedAnimation += WaitTextAnimation;
					_textNumberController.MakeVisible(false);
					yield return new WaitUntil(() => buttonsVisible == buttonsToHide && textVisible);
					GameState = GameStateEnum.PrepareGame;
					break;

					void WaitButtonsAnimation(bool visible)
					{
						Debug.Log("ShowButtonFinished");
						if (_textNumberController.NumberModel.OnVisibleFinishedAnimation != null)
							_textNumberController.NumberModel.OnVisibleFinishedAnimation -= WaitButtonsAnimation;
						buttonsVisible++;
					}

					void WaitTextAnimation(bool visible)
					{
						Debug.Log("ShowNumberFinished");
						if (_textNumberController.NumberModel.OnVisibleFinishedAnimation != null)
							_textNumberController.NumberModel.OnVisibleFinishedAnimation -= WaitTextAnimation;
						textVisible = true;
					}
				}
				default:
					throw new ArgumentOutOfRangeException();
			}

			//TODO: Remove this when all states are done
			yield return null;
		}
	}
}